// Using this to test

// shape functions
function drawLeft () {
  var canvas = document.getElementById('canvasL')
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d')
    // triangle
    canvas.addEventListener('click', function (event) {
      var tri = canvas.getBoundingClientRect()
      var xL = event.clientX - tri.left
      var yL = event.clientY - tri.top
      console.log('left x: ' + xL + ' left y: ' + yL)
    }, false)
    ctx.fillStlye = '#FFFFFF'
    ctx.beginPath()
    ctx.moveTo(440, 420)
    ctx.lineTo(280, 420)
    ctx.lineTo(360, 300)
    ctx.fill()
  }
  // square
  if (canvas.getContext) {
    ctx.fillStyle = '#F5F5DC'
    ctx.fillRect(24, 24, 120, 120)
    ctx.clearRect(48, 48, 72, 72)
    ctx.strokeRect(35, 35, 98, 98)
    ctx.strokeRect(48, 48, 72, 72)
  }
  // circle
  if (canvas.getContext) {
    ctx.beginPath()
    ctx.arc(340, 80, 50, 0, Math.PI * 2)
    ctx.fillStyle = '#7FFFD4'
    ctx.fill()
    ctx.closePath()
  }
}

function drawRight () {
  var canvas = document.getElementById('canvasR')

  function triangleR () {
    canvas.addEventListener('click', function (event) {
      var tri = canvas.getBoundingClientRect()
      var xR = event.clientX - tri.left
      var yR = event.clientY - tri.top
      console.log('right x: ' + xR + ' right y: ' + yR)
    }, false)

    if (canvas.getContext) {
      var ctx = canvas.getContext('2d')
      // triangle
      ctx.fillStyle = '#7FFFD4'
      ctx.beginPath()
      ctx.moveTo(440, 420)
      ctx.lineTo(280, 420)
      ctx.lineTo(360, 300)
      ctx.fill()
    }
  }
  // square
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d')
    ctx.fillStyle = '#F5F5DC'
    ctx.fillRect(24, 24, 120, 120)
    ctx.clearRect(48, 48, 72, 72)
    ctx.strokeRect(35, 35, 98, 98)
    ctx.strokeRect(48, 48, 72, 72)
  }
  // circle
  if (canvas.getContext) {
    ctx.beginPath()
    ctx.arc(340, 80, 50, 0, Math.PI * 2)
    ctx.fillStyle = '#7FFFD4'
    ctx.fill()
    ctx.closePath()
  }

  triangleR()
}
// shape detection

function draw () {
  drawLeft()
  drawRight()
}

draw()
